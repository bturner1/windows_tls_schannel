TLS/SCHANNEL configuration for Windows Server 2012r2 and 2016
======

TLS and SCHANNEL configuration for Windows servers requires a rigid testing regimen in order to avoid breaking communication between actively deployed services.  There is no mix of configuration that can be deployed without prior validation of your current application requirements.  DO NOT DEPLOY THESE SETTINGS WITHOUT TESTING YOUR SERVICE REQUIREMENTS.  They are likely to break some client connections in environments with windows operating systems older than Windows 10 and Windows Server 2016.

Understand what you are doing before blindly applying these configurations to your systems.  These configurations have been tested in a very specific environment, and other environments are certain to have slightly different requirements.  Applying these settings to Windows server will impact both the client connections that a server makes to other hosts (for example, to MSSQL services), and to clients connecting to services on the server (for example, https connections to IIS).

These are valuable explanations of TLS and SCHANNEL ciphers, as well as some tools to build and test configurations.  Read these, and more.

* Microsoft's schannel cipher documentation:
    <https://docs.microsoft.com/en-us/windows/win32/secauthn/cipher-suites-in-schannel>
* Enable schannel logging events in Windows server:
    <https://support.microsoft.com/en-us/help/260729/how-to-enable-schannel-event-logging-in-iis>
* Qualsys TLS deployment practices:
    <https://github.com/ssllabs/research/wiki/SSL-and-TLS-Deployment-Best-Practices>
* Perfect forward secrecy with TLS 1.2:
    <https://www.hass.de/content/setup-microsoft-windows-or-iis-ssl-perfect-forward-secrecy-and-tls-12>
* Test your TLS configurations:
    <https://testssl.sh/>
* Convert Windows .reg files to xml that can be imported into Group Policy preferences
    <http://chentiangemalc.wordpress.com/2014/07/02/importing-reg-files-into-group-policy-preferences/>

The configuration files in this repo are a current configuration that allow only TLS 1.2 with reasonably secure cipher suites.  Other combinations can provide higher levels of secure communications between your Windows services, but older clients may not support more secure ciphers.

Files:

* TLS_1.2_WindowsServer2012r2.reg  
    Registry merge file for Windows Server 2012r2  
* TLS_1.2_Server2012r2_gp_reg.xml  
    XML file that can be pasted into registry settings in Group Policy Preferences for Windows Server 2012r2  
* TLS_1.2_WindowsServer2016.reg  
    Registry merge file for Windows Server 2016  
* TLS_1.2_WindowsServer2016_gp_reg.xml  
    XML file that can be pasted into registry settings in Group Policy Preferences for Windows Server 2016  
